package net.poundex.nodehelper;

import jakarta.validation.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import java.nio.file.Path;
import java.util.Optional;


@ConfigurationProperties(prefix = "nodehelper")
@Validated
public record NodeHelperProperties(
		@NotBlank String vpcId, 
		@NotBlank String allocationId,
		Path kubeconfig,
		@NotBlank String tagName,
		@NotBlank String tagValue
) { 
	public Optional<Path> maybeKubeconfig() {
		return Optional.ofNullable(kubeconfig);
	}
}

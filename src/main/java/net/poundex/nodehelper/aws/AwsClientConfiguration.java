package net.poundex.nodehelper.aws;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ec2.Ec2Client;

@Configuration
class AwsClientConfiguration {
	@Bean
	public Ec2Client ec2Client() {
		return Ec2Client.builder().region(Region.EU_WEST_2).build();
	}
}

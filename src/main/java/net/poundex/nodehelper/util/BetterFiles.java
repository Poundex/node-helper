package net.poundex.nodehelper.util;

import io.vavr.control.Try;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class BetterFiles {
	public static String readString(Path path) {
		return Try.of(() -> Files.readString(path))
				.recover(IOException.class, x -> {
					throw new UncheckedIOException(x);
				})
				.get();
	}
}

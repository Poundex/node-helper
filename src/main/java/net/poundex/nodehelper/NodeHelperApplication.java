package net.poundex.nodehelper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class NodeHelperApplication {
	public static void main(String[] args) {
		SpringApplication.run(NodeHelperApplication.class);
	}
}

package net.poundex.nodehelper.k8s;

import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientBuilder;
import lombok.RequiredArgsConstructor;
import net.poundex.nodehelper.NodeHelperProperties;
import net.poundex.nodehelper.util.BetterFiles;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
class K8sClientConfiguration {
	
	private final NodeHelperProperties nodeHelperProperties;
	
	@Bean
	public KubernetesClient kubernetesClient() {
		KubernetesClientBuilder kubernetesClientBuilder = new KubernetesClientBuilder();
		nodeHelperProperties.maybeKubeconfig().ifPresent(k -> kubernetesClientBuilder
				.withConfig(Config.fromKubeconfig(BetterFiles.readString(k))));
		
		return kubernetesClientBuilder.build();
	}
}

package net.poundex.nodehelper;

import io.fabric8.kubernetes.client.KubernetesClient;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.AssociateAddressRequest;
import software.amazon.awssdk.services.ec2.model.AssociateAddressResponse;
import software.amazon.awssdk.services.ec2.model.Instance;
import software.amazon.awssdk.services.ec2.model.InstanceStateName;

import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
class HelpNodeJob implements ApplicationListener<ApplicationReadyEvent> {
	
	private final NodeHelperProperties nodeHelperProperties;
	private final Ec2Client ec2Client;
	private final KubernetesClient kubernetesClient;
	
	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		cleanUpNodes();
		associateAddress();
		synchronized (this) {
			Try.run(this::wait).get();
		}
	}
	
	private void associateAddress() {
		List<Instance> instances = ec2Client
				.describeInstances()
				.reservations()
				.stream()
				.flatMap(r -> r.instances().stream()
						.filter(inst -> inst.state().name() == InstanceStateName.RUNNING)
						.filter(inst -> nodeHelperProperties.vpcId().equals(inst.vpcId()))
						.filter(inst -> inst.tags().stream().anyMatch(t -> 
								t.key().equals(nodeHelperProperties.tagName()) 
										&& t.value().equals(nodeHelperProperties.tagValue()))))
				.toList();

		if (instances.size() != 1)
			throw new IllegalStateException("Failed to find exactly one instance: " +
					instances.stream().map(Instance::instanceId).toList());

		Instance instance = instances.get(0);

		log.info("Found instance: {}", instance.instanceId());

		AssociateAddressResponse associateAddressResponse = ec2Client.associateAddress(AssociateAddressRequest.builder()
				.allocationId(nodeHelperProperties.allocationId())
				.instanceId(instance.instanceId())
				.build());

		log.info("Address associated: {}", associateAddressResponse.associationId());
	}
	
	private void cleanUpNodes() {
		kubernetesClient.resourceList(kubernetesClient.nodes().list().getItems().stream()
						.filter(n -> ! n.getStatus().getConditions().stream()
								.filter(c -> c.getType().equals("Ready"))
								.findFirst()
								.orElseThrow()
								.getStatus()
								.equals("True"))
						.peek(n -> log.info("Node {} is NotReady", n.getMetadata().getName()))
						.toList())
				.delete();
	}
}
